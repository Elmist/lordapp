﻿using BigData.Api.Configs.Factories.Interfaces;
using BigData.Api.Configs.Settings;
using BigData.Api.Controllers;
using BigData.Domain.Models.Identity;
using BigData.Domain.Persistence;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Threading.Tasks;

namespace BigData.Api.FeatureLinkedIn.Controllers
{
    [Route("account")]
    [ApiController]
    //   [Authorize]
    public class RegisterController : ControllerBase
    {
        private readonly IOptions<LinkedInAuthRequestOptions> _linkedinOptions; 
        private readonly UserManager<UserAccount> _userManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly ITokenFactory _tokenFactory;
        private readonly SignInManager<UserAccount> _signInManager;

        public RegisterController(IOptions<LinkedInAuthRequestOptions> linkedinOptions, UserManager<UserAccount> userManager, RoleManager<UserRole> roleManager, ITokenFactory tokenFactory, SignInManager<UserAccount> signInManager)
        {
            _linkedinOptions = linkedinOptions;
            _userManager = userManager;
            _roleManager = roleManager;
            _tokenFactory = tokenFactory;
            _signInManager = signInManager;
        }

        [Route("Debug_content")]
        public IActionResult content()
        {
            throw new NotImplementedException();
           // return Ok(_linkedinOption tostring());
        }

        [Route("test")]
        [HttpGet]
        public async Task<IActionResult> Test( /* Model here */)
        {   

            // Trying to fake input ... Delete this model
            var model = new RegisterAction().GetModel();

            //Default Role TODO: needs a global definition
            var role = "user";

            // UserAccount and UserRoles are custom database models found in BigData.Domain 
            //Accept that managers are 50% relative 

            //dbo.AspNetUserLogin wants .net official type UserLoginInfo (provider, their provided unique Id, display name) TODO: To Make controller generic first peramiter, needs to be provided with model 
            var externalUserLoginInfo = new UserLoginInfo("LinkedIn", model.ExternalAccountId, model.LastName);

            //Exernal providers have special indexes
            var newUser = new UserAccount()
            {
                Headline = model.Headline,
                UserName = model.LastName,
                Email = model.Email,
                SiteStandardProfileRequest = model.SiteStandardProfileRequest
            };

            // checking if user already has an account
            var user = await _userManager.FindByNameAsync(model.LastName);
            if (user == null)
            {   // Make an account that we can link to other tables
                var makeUser = await _userManager.CreateAsync(newUser);
                //If we cant do that we are screwed
                if (!makeUser.Succeeded) return StatusCode(500);
            }
            //We hopfully have a base account now so checking if we just made one, or if it's reeoccuring visitor 
            var userd = await _userManager.FindByLoginAsync("Linkedin", model.ExternalAccountId);
            // If we just made the account userd will be null. So preparing for next if statment we need to get the user we jus made.
            var userSecondChance = await _userManager.FindByNameAsync(model.LastName);
            // if we just made the account we need to create and link it to third party uniqe data in dbo.AspNetUserLogin. 
            if (userd == null) await _userManager.AddLoginAsync(userSecondChance, externalUserLoginInfo);

            // All data is lost so the fist person coming this way will create the role. Using a constructor seems easier and dynamic compared messing with DBcontext.
            if (await _roleManager.FindByNameAsync(role) == null) await _roleManager.CreateAsync(new UserRole(role, "Base User", DateTime.Now));

            // just to be sure we have fully a configured third party user
            var userd2 = await _userManager.FindByLoginAsync("Linkedin", model.ExternalAccountId);

            // Why give null roles and tokens. role + token * null = 0
            if (userd2 == null) return StatusCode(500);
            // So now we can give the role that was there or we just made
            await _userManager.AddToRoleAsync(userd2, role);

            // we put user with third party credentials through the token generator. The token inludes a claim that will pass [authorize(roles = "user")]
            var newToken = new JwtSecurityTokenHandler().WriteToken(await _tokenFactory.GetTokenAsync(userd2));

            //Returns the token and whatever u want. Current map tests if the same user that was put in, is put out. 
            var mappedResult = new LoginPostModel()
            {
                Name = userd2.UserName,
                Token = newToken
            };

            return Ok(mappedResult);
        }
    }
}

