﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BigData.Api.FeatureLinkedIn.models
{
    public class ExternalUserPostModel
    {

        public string FirstName { get; set; } = "Glenn";
        public string Headline { get; set; } = "Student på Göteborgs univeritet";
        public string Email { get; set; } = "LinkedIn@placeholder.com";
        public string ExternalAccountId { get; set; } = "rW546J1Sgn";
        public string LastName { get; set; } = "Svanberg";
        public string SiteStandardProfileRequest { get; set; } = null;
    }
}
