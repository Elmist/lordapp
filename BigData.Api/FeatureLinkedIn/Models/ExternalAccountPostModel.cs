﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BigData.Domain.Models;

namespace BigData.Api.FeatureLinkedIn.models
{
    public class ExternalAccountPostModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string Headline { get; set; }
        public string ExternalAccountId { get; set; }
        public string LastName { get; set; }
        public string SiteStandardProfileRequest { get; set; }

    }
}
