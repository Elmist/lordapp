﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BigData.Api.Models.Post;
using BigData.Domain.Models;
using Microsoft.AspNetCore.Identity;
using BigData.Domain.Models.Identity;
using Microsoft.AspNetCore.Authorization;
using BigData.Api.FeatureLinkedIn;
using BigData.Domain.Persistence;
using BigData.Domain.Models.FeatureLinkedIn;
using System.Diagnostics;
using BigData.Api.FeatureLinkedIn.Interfaces;

namespace BigData.Api.Controllers
{   
    [Route("account")]
    [ApiController]
 //   [Authorize]
    public class RegisterController : ControllerBase
    {
        private readonly UserManager<UserAccount> _userManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly IExternalUserRepository _externalUserRepoitory;
        private readonly DataContext _context;
        private readonly SignInManager<UserAccount> _signInManager;

        public RegisterController(UserManager<UserAccount> userManager, RoleManager<UserRole> roleManager, IExternalUserRepository externalUserRepoitory, DataContext context, SignInManager<UserAccount> signInManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _externalUserRepoitory = externalUserRepoitory;
            _context = context;
            _signInManager = signInManager;
        }

        [Route("test")]
        [HttpGet]
        public async Task<IActionResult> Test()
        {
            var testPeramiters = new ExternalUserPostModel() {};

            var externalDetail = new ExternalUserDetails()
            {
                FirstName = testPeramiters.FirstName,
                Headline = testPeramiters.Headline,
                ExternalAccountId = testPeramiters.ExternalAccountId,
                LastName = testPeramiters.LastName,
                SiteStandardProfileRequest = testPeramiters.SiteStandardProfileRequest                
            };
            var newUserName = new String(testPeramiters.FirstName + testPeramiters.LastName);

            var newUser = new UserAccount()
            {
                UserName = newUserName,
                Email = "Linkedin@Placeholder.com",
                Headline = testPeramiters.Headline,
                SiteStandardProfileRequest = testPeramiters.SiteStandardProfileRequest

            };

            var externalUserLoginInfo = new UserLoginInfo("LinkedIn", testPeramiters.ExternalAccountId, newUserName);




            //var result = await _userManager.CreateAsync(newUser);
            //var getSavedUSer = await _userManager.FindByNameAsync(newUserName);
            // var result = await _userManager.AddLoginAsync(getSavedUSer, externalUserLoginInfo);

            var user = await _userManager.FindByLoginAsync("Linkedin", "rW546J1Sgn");
            return Ok(user);
          
        }

        [Route("register")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterPostModel registerPostModel)
        {

            var userRole = "user";

            var user = new UserAccount()
            {
                UserName = registerPostModel.Email,
                Email = registerPostModel.Email
            };

            if(await _roleManager.FindByNameAsync(userRole) == null) await _roleManager.CreateAsync(new UserRole(userRole, "Base User", DateTime.Now));

            var result = await _userManager.CreateAsync(user, registerPostModel.Password);

            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, userRole);

                return Ok(result);
            }

            var userId1 = user.Id;

           return BadRequest("Register recieved bad information");

           

        }

    }
}