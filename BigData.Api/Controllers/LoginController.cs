﻿using BigData.Api.Configs.Factories.Interfaces;
using BigData.Api.Models.LinkedInUriOptions;
using BigData.Domain.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Threading.Tasks;

namespace BigData.Api.Controllers
{
    [Route("account")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly SignInManager<UserAccount> _signInManager;
        private readonly UserManager<UserAccount> _userManager;
        private readonly ITokenFactory _tokenFactory;
        private readonly IHttpClientFactory _httpClient;
       // private readonly ILinkedAuthRequestsFactory _linkedAuthRequests;



        public LoginController(
            SignInManager<UserAccount> signInManager,
            UserManager<UserAccount> userManager,
            ITokenFactory tokenFactory,
            IHttpClientFactory httpClient)
           // ILinkedAuthRequestsFactory linkedAuthRequests)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _tokenFactory = tokenFactory;
            _httpClient = httpClient;
           // _linkedAuthRequests = linkedAuthRequests;

        }

        [Route("login")]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginPostModel login)
        {
            try
            {
                var result = await _signInManager.PasswordSignInAsync(login.Email, login.Password, false, false);

                if (!result.Succeeded) return BadRequest("Bad Credentials");


                var user = await _userManager.FindByNameAsync(login.Email);

                var token = new JwtSecurityTokenHandler().WriteToken(await _tokenFactory.GetTokenAsync(user));

                if (token == null) return BadRequest("token");

                var mappedResult = new LoginPostModel()
                {
                    Token = token,
                    Email = user.Email

                };

                return Ok(mappedResult);
            }
            catch (Exception e)
            {
                Debug.Write(e.StackTrace);
            }

            return BadRequest("Unable to authroize");
        }

        //LINKED IN STUFF

        [Route("call")]
        [HttpGet]
        public IActionResult Call()
        {
         //   var request = _linkedAuthRequests.LinkedAuthRequests();

            return Redirect(""/*request*/);
        }



        [Route("linkedin")]
        [HttpGet]
        public async Task<IActionResult> CallbackAsync([FromQuery] LinkedInUriOptions linkedinResponseOne)
        {

            var theCode = new LinkedInUriOptions { Code = linkedinResponseOne.Code };

            var client = _httpClient.CreateClient();

            var response = await client.GetStringAsync("");

            return Ok(response);

        }

    }
}