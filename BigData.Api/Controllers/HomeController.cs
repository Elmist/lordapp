﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;



namespace BigData.Api.Controllers
{
    [Route("")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        [Authorize(Roles = "user")] // (AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "user")
        [Route("home")]
        [HttpGet]
        public IActionResult GetFrontEnd()
        {
            return Ok(Redirect("http://localhost:8080/"));
        }
    }
}