﻿using BigData.Api.Configs.Factories;
using BigData.Api.Configs.Factories.Interfaces;
using BigData.Api.Configs.settings;
using BigData.Api.Configs.Settings;
//using BigData.Api.Configs.Settings.Interfaces;
using BigData.Domain.Models.Identity;
using BigData.Domain.Persistence;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using BigData.Api.FeatureLinkedIn;


namespace BigData.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // Routing
            services.AddRouting(options =>
                 options.LowercaseUrls = true);

            services.AddIdentity<UserAccount, UserRole>()
             .AddEntityFrameworkStores<DataContext>()
             .AddDefaultTokenProviders();

            services.AddHttpClient();

            //EntityFramework
            services.AddDbContext<DataContext>(options =>
                       options.UseSqlServer(Configuration.GetConnectionString("Default")));

            services.Configure<IdentityOptions>(options =>
                   {
                       options.Password.RequiredUniqueChars = 0;
                       options.Password.RequireNonAlphanumeric = false;
                       options.Password.RequireUppercase = false;
                       options.Password.RequireDigit = false;

                       options.User.RequireUniqueEmail = true;
                   });



            //Transients
            services.AddTransient<ITokenFactory, TokenFactory>();
            //  services.AddTransient<ILinkedAuthRequestsFactory, LinkedAuthUriFactory>();

            //Options
            services.Configure<JwtOptions>(Configuration.GetSection("Jwt"));
            services.Configure<LinkedInAuthRequestOptions>(Configuration.GetSection("LinkdedIn"));
            

            //Security
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.IncludeErrorDetails = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,  //Configuration.GetSection("Jwt").GetValue<string>("Audience
                    ValidIssuer = Configuration.GetSection("Jwt").GetValue<string>("Issuer1"),
                    ValidAudience = Configuration.GetSection("Jwt").GetValue<string>("Audience1"),
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetSection("Jwt").GetValue<string>("Secret")))
                };
            }).AddLinkedIn(options =>
            {
                options.ClientId = Configuration.GetSection("LinkedIn").GetValue<string>("ClientID");
                options.ClientSecret = Configuration.GetSection("LinkedIn").GetValue<string>("ClientSecret");
            });
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(builder =>
                builder.WithOrigins("http://localhost:8080").AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

            app.UseAuthentication();
            app.UseMvc();

        }
    }
}
