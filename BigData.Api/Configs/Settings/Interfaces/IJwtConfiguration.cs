namespace BigData.Api.Configs.Settings.Interfaces
{

        public interface IJwtConfiguration
        {
             string Secret { get; set; }
             string Issuer1 { get; set; }
             string Audience { get; set; }
             int Expiry { get; set; }
        }
    
}