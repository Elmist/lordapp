//using BigData.Api.Configs.Settings.Interfaces;

namespace BigData.Api.Configs.settings
{
    public class JwtOptions //: IJwtConfiguration
    {
        public string Secret { get; set; }
        public string Issuer1 { get; set; }
        public string Audience1 { get; set; }
        public int Expiry { get; set; }
    }
}