﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BigData.Api.Configs.Factories.Interfaces
{
    public interface ILinkedAuthUriFactory
    {
        string LinkedAuthRequests();
        string GetToken(string code);
    }
}
