﻿using BigData.Domain.Models.Identity;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace BigData.Api.Configs.Factories.Interfaces
{
    public interface ITokenFactory
    {
        Task<JwtSecurityToken> GetTokenAsync(UserAccount userAccount);
    }
}
