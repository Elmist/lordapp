﻿using BigData.Api.Configs.Factories.Interfaces;
using BigData.Api.Configs.settings;
//using BigData.Api.Configs.Settings.Interfaces;
using BigData.Domain.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BigData.Api.Configs.Factories
{
    public class TokenFactory : ITokenFactory
    {

        private readonly IOptions<JwtOptions> _configuration;
        private readonly UserManager<UserAccount> _userManager;

        public TokenFactory(IOptions<JwtOptions> configuration, UserManager<UserAccount> userManager)
        {
            _configuration = configuration;
            _userManager = userManager;
        }

        public async Task<JwtSecurityToken> GetTokenAsync(UserAccount user)
        {

            var claims = new List<Claim>
            {};

            var userRoles = await _userManager.GetRolesAsync(user);
            claims.AddRange(userRoles.Select(Role => new Claim(ClaimTypes.Role, Role)));


            var securityKey = new SymmetricSecurityKey(Encoding.Default.GetBytes(_configuration.Value.Secret));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

            return new JwtSecurityToken(
                _configuration.Value.Issuer1,
                _configuration.Value.Audience1,
                claims,
                expires: DateTime.UtcNow.AddMinutes(_configuration.Value.Expiry),
                signingCredentials: signingCredentials);
        }
    }
}
