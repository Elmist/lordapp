﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BigData.Api.Models.LinkedInUriOptions
{
    public class LinkedInUriOptions
    {
        public string Grant_type { get; set; }
        public string Code { get; set; }
        public string Redirect_Uri { get; set; }
        public int Client_Id { get; set; }
        public string Client_secret{ get; set; }
    }
}
