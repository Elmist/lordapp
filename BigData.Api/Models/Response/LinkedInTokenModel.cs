﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BigData.Api.Models.Response
{
    public class ResonLinkedInTokenModel

    {
        public string Access_token { get; set; }
        public string Expires_in { get; set; }

    }
}
