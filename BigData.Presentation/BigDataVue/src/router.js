import Router from "vue-router";

export default new Router({
                           
                           routes: [
                            {
                             path: "/Banner",
                             name: "Banner",
                             component: () => import("./components/Banner.vue")
                            },
                            {
                             path: "/Home",
                             name: "Home",
                             component: () => import("./App.vue")
                            },
                            {
                             path: "/Register",
                             name: "Register",
                             component: () => import("./components/Register.vue")
                            },
                            {
                             path: "/Login",
                             name: "Login",
                             component: () => import("./components/Login.vue")
                            }
                           ],
                           mode: "history"
                          });
