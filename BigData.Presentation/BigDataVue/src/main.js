import Vue from "vue";
import App from "./App.vue";
import Router from "vue-router";
import {store} from "./store";
import Vuex from "vuex"
import router from "./router";

Vue.use(Router);
Vue.use(Vuex);

new Vue({
         Router,
         router,
         store,
         Vuex,
         render: h => h(App)
        }).$mount('#app');
