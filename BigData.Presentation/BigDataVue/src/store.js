import Vue from 'vue';
import Vuex from 'vuex';
import Axios from 'axios';

Vue.use(Vuex);


export const store =
  
  new Vuex.Store({
                  state: {
                   loggedIn: false,
                   email: '',
                   password: '',
                   token: '',
                   baseLogin: 'https://localhost:44395/account/login',
                  },
                  mutations: {
                   setLogout(state) {
                    state.loggedIn = false;
                   },
                   setToken(state, value) {
                    state.token = value;
                    state.loggedIn = true;
                   }
                  },
                  actions: {
                   doLogout({commit}) {
                    commit('setLogout')
                   },
                   doToken({commit}, value) {
                    commit('setToken', value);
                   }
                  },
                  getters: {
                   getEmail: state => state.Email,
                   getToken: state => state.token
                  },
   
                 });

