﻿using System;
using System.Collections.Generic;
using System.Text;
using BigData.Domain.Models;
using BigData.Domain.Models.FeatureLinkedIn;
using BigData.Domain.Models.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BigData.Domain.Persistence
{
    public class DataContext : IdentityDbContext<UserAccount, UserRole, string>
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options){ }
        

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ExternalUserDetails>().HasIndex(i => i.ExternalAccountId).IsUnique();
        }


        public DbSet<UserAccount> UserAccounts { get; set; }
        public DbSet<ExternalUserDetails> ExternalUserDetailss { get; set; }


    }
}
