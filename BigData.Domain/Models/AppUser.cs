﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BigData.Domain.Models
{
    public class AppUser : IdentityUser 
    {
        //[Key]
        //public int UserId { get; set; }
       

        public string Name { get; set; }
        public string ThumbnailUrl { get; set; }

        //    public ContactStatus Status { get; set; } 
    }
    //public enum ContactStatus
    //{
    //    Submitted,
    //    Approved,
    //    Rejected
    //}
}
