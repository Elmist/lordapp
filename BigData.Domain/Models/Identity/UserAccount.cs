﻿using BigData.Domain.Models.FeatureLinkedIn;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BigData.Domain.Models.Identity
{
    public class UserAccount : IdentityUser
    {
        public string Password { get; set; }

        //LinkedIn
        public string Headline { get; set; }
        public string SiteStandardProfileRequest { get; set; }

    }
}
