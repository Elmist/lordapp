﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace BigData.Domain.Models.Identity
{

    public class UserRole : IdentityRole

    {
        public UserRole() : base(){}

        public UserRole(string roleName) : base(roleName) { }

        public UserRole(string roleName, string description, DateTime creationDate) : base(roleName)
        {

            this.Description = description;
            this.CreationDate = creationDate;

        }


        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
    }
}



