﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace BigData.Domain.Models.FeatureLinkedIn
    {
    public class ExternalUserDetails
    {
        [Key]
        public string ExternalAccountId { get; set; }
        public string FirstName { get; set; }
        public string Headline { get; set; }
        public string LastName { get; set; }
        public string SiteStandardProfileRequest { get; set; }
    }
}
